document.addEventListener("DOMContentLoaded", () => {
  const socket = io("ws://localhost:3000").connect();

  // Login
  const loginSection = document.getElementById("loginSection");
  const userInput = document.getElementById("userInput");
  const loginButton = document.getElementById("loginButton");

  // Chat
  const chatSection = document.getElementById("chatSection");
  const messagesContainer = document.getElementById("messagesContainer");
  const sendMessageButton = document.getElementById("sendMessage");
  const messageInput = document.getElementById("messageInput");

  // Opcional
  const toggleEmojiPicker = document.getElementById("toggleEmojiPicker");
  const emojiPicker = document.getElementById("emojiPicker");

  let username = "";

  loginButton.addEventListener("click", (e) => {
    e.preventDefault();
    username = userInput.value.trim();
    if (username) {
      loginSection.style.display = "none";
      chatSection.style.display = "block";
      socket.emit("join", { username });
    } else {
      alert("Ingresa un nombre de usuario");
    }
  });

  socket.on("previousMessages", (messages) => {
    messages.forEach((message) => {
      appendMessage(`${message.username}: ${message.message}`);
    });
  });

  socket.on("receivedMessage", (message) => {
    appendMessage(`${message.username}: ${message.message}`);
  });

  sendMessageButton.addEventListener("click", (e) => {
    e.preventDefault();
    const messageText = messageInput.value.trim();
    if (messageText) {
      const message = { username, message: messageText };
      socket.emit("sendMessage", message);
      appendMessage(`${message.username}: ${message.message}`);
      messageInput.value = "";
    } else {
      alert("Ingresa un mensaje antes...");
    }
  });

  function appendMessage(message) {
    const messageElement = document.createElement("div");
    messageElement.textContent = message;
    messagesContainer.appendChild(messageElement);
  }

  toggleEmojiPicker.addEventListener("click", () => {
    emojiPicker.style.display =
      emojiPicker.style.display === "none" ? "block" : "none";
  });

  emojiPicker.addEventListener("emoji-click", (event) => {
    messageInput.value += event.detail.unicode;
  });

  document.addEventListener("click", (event) => {
    if (
      !event.target.matches("#toggleEmojiPicker, emoji-picker, emoji-picker *")
    ) {
      emojiPicker.style.display = "none";
    }
  });
});
