import MessageModel from "../models/messages.model";

export default async (socket) => {
  socket.on("join", async (body) => {
    const { username } = body;
    const previousMessages = await MessageModel.find();
    socket.emit("previousMessages", previousMessages);
    console.log(`Se unio el usuario ${username} con el id -> ${socket.id}`);
  });
};
