import MessageModel from "../models/messages.model";

export default async (socket) => {
  socket.on("sendMessage", async (body) => {
    const record = MessageModel(body);
    await record.save();

    socket.broadcast.emit("receivedMessage", body);
  });
};
