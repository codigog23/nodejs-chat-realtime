export default async (socket) => {
  socket.on("disconnect", () => {
    console.log(`Se desconecto del socketIO -> ${socket.id}`);
  });
};
