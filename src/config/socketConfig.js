import { Server } from "socket.io";
import eventModules from "../events";

class SocketConfig {
  constructor(server) {
    this.socket = new Server(server, {
      cors: {
        origin: "*",
      },
    });
  }

  async listen() {
    this.socket.on("connection", async (socket) => {
      // Conexion persistente
      console.log(`Conexión con socketIO -> ${socket.id}`);
      await eventModules(socket);
    });
  }
}

export default SocketConfig;
