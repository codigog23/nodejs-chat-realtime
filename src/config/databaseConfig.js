import { connect } from "mongoose";

export default async () => {
  const PORT = process.env.DB_PORT;

  const databases = {
    development: {
      uri: `mongodb://${process.env.DB_HOST}:${PORT}/${process.env.DB_DATABASE}`,
      options: {},
    },
    production: {
      uri: `${process.env.DB_URI}`,
      options: {},
    },
  };

  const { uri, options } = databases[process.env.NODE_ENV];

  try {
    await connect(uri, options);
    console.log(`MongoDB running ${PORT} 🌎`);
  } catch (error) {
    console.log(`MongoDB error -> ${error}`);
  }
};
