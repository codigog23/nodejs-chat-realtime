import express from "express";
import morgan from "morgan";
import { errors } from "celebrate";
import routersInit from "../routers";
import { StatusCodes } from "http-status-codes";
import fileUpload from "express-fileupload";
import { createServer } from "http";
import SocketConfig from "./socketConfig";

export class ExpressConfig {
  constructor() {
    this.port = process.env.PORT;
    this.app = express();
    this.server = createServer(this.app);

    this.middlewares();
    this.routers();
  }

  middlewares() {
    this.app.use(express.json());
    this.app.use(morgan("dev"));
    this.app.use(fileUpload({ debug: true }));
  }

  routers() {
    routersInit(this.app);
    this.app.use(errors({ statusCode: StatusCodes.BAD_REQUEST }));
  }

  async socketServerInit() {
    const socket = new SocketConfig(this.server);
    await socket.listen();
  }

  async listen() {
    this.server.listen(this.port, () => {
      console.log(`Express running ${this.port} 🚀`);
    });
  }
}
