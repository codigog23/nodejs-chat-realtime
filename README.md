# Chat Realtime

- Entorno de ejecucion

## Variables de entorno (.env)

```py
DB_DATABASE=''
DB_USERNAME=''
DB_PASSWORD=''
DB_HOST=''
DB_PORT=5432

TZ='America/Lima'
PORT=3000
NODE_ENV=development
```

## Entidades (Modelos)

- Mensajes

| campo    | tipo    | opciones |
| -------- | ------- | -------- |
| message  | VARCHAR |          |
| username | VARCHAR |          |
